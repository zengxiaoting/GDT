package com.example.elvn.my2;

import android.app.Activity;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

/**
 * Created by Elvn on 2015/12/5.
 */
public class TestAc extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_plan);

        Expendadapter eeee = new Expendadapter();
        Log.i("BugMeNot", eeee.getGroupCount() + "  is count" + eeee.getChildrenCount(1));
        
    }
    class Expendadapter extends BaseExpandableListAdapter {
        private String[] group = new String[] {"视频直播","广播直播"};
        private String[][] child = new String[][] {
                {"发射端","中转端","接收端"},
                {"发射端","中转端","接收端"}
        };

        @Override
        public int getGroupCount() {
            return group.length;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return child[groupPosition].length;
        }

        @Override
        public Object getGroup(int groupPosition) {
            return group[groupPosition];
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return child[groupPosition][childPosition];
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            String string = group[groupPosition];
            return getGenericView(string);

        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            String string = child[groupPosition][childPosition];
            return getGenericView(string);

        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

    }
    private TextView getGenericView(String string )
    {
        AbsListView.LayoutParams  layoutParams =new AbsListView.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        TextView  textView =new TextView(this);
        textView.setLayoutParams(layoutParams);
        textView.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
        textView.setPadding(40, 0, 0, 0);
        textView.setText(string);
        return textView;
    }
}
