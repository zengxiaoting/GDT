package com.example.elvn.my2.Fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.elvn.my2.JsonType;
import com.example.elvn.my2.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import net.tsz.afinal.FinalBitmap;

import org.apache.http.Header;
import org.json.JSONArray;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by Elvn on 2015/11/10.
 */
public class Fragment_search extends Fragment {
    final String arr1[] = {"item_id", "item_name", "item_seller_id", "item_img", "item_price",
            "item_quantity", "item_size", "item_tag", "item_info", "item_desc"};
    FancyButton btn_search;
    ImageButton re;
    ImageView di;
    EditText name_search;
    TextView hint;
    TextView title;
    Spinner tag_search;
    Spinner price;
    Handler mhandler;
    String name_value, tag_value, low_value, high_value;
    ListView lv;
    List<String> tag_list = new ArrayList<String>();
    List<String> price_list = new ArrayList<String>();
    private FragmentTransaction ft;
    private FragmentManager manager;
    private ArrayAdapter<String> spinner_adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View contentView = inflater.inflate(R.layout.fragment_search, null);
        name_search = (EditText) contentView.findViewById(R.id.search_name);
        tag_search = (Spinner) contentView.findViewById(R.id.tag);
        price = (Spinner) contentView.findViewById(R.id.price);
        btn_search = (FancyButton) contentView.findViewById(R.id.search_button);
        hint = (TextView) contentView.findViewById(R.id.search_hint);
        hint.setVisibility(View.VISIBLE);
        lv = (ListView) contentView.findViewById(R.id.search_list);
        re = (ImageButton) getActivity().findViewById(R.id.refreshbtn);
        di = (ImageView)getActivity().findViewById(R.id.imageView2);
        ImageButton back = (ImageButton) getActivity().findViewById(R.id.backbtn);
        ImageView dii = (ImageView)getActivity().findViewById(R.id.imageView);
        dii.setVisibility(View.VISIBLE);
        back.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manager = getFragmentManager();
                Fragment_home fragment_info = new Fragment_home();
                ft = manager.beginTransaction();
                ft.setCustomAnimations(R.anim.slide_left_in,R.anim.slide_right_out);
                ft.replace(R.id.realtabcontent, fragment_info);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        title = (TextView)getActivity().findViewById(R.id.title_txt);
        title.setText("搜索");
        re.setVisibility(View.GONE);
        di.setVisibility(View.GONE);

        initial();

//       handle the result of the search
        mhandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                Bundle b = msg.getData();
                final List<Map<String, Object>> mDataList = getDataFromBundle(b);
                if (b.isEmpty())
                    Log.i("msg", "Bundle doesn`t get data");
                else {
                    Log.i("msg", "Bundle gets data");
                    try {
//initial adapter by binding to the home_item.xml
                        hint.setVisibility(View.GONE);
                        SimpleAdapter adapter = new SimpleAdapter(getActivity(), mDataList,
                                R.layout.home_item,
                                new String[]{"item_name", "item_seller_id", "item_img",
                                        "item_price", "item_id", "item_quantity", "item_size",
                                        "item_tag", "item_info", "item_desc"},
                                new int[]{R.id.itemname, R.id.itemsellerid, R.id.itemimg, R.id.itemprice});

                        adapter.setViewBinder(new SimpleAdapter.ViewBinder() {
                            @Override
                            public boolean setViewValue(View view, Object data,
                                                        String textRepresentation) {
                                if (view instanceof ImageView && data instanceof String) {
                                    ImageView iv = (ImageView) view;
                                    FinalBitmap bitmap = FinalBitmap.create(getActivity());
                                    bitmap.display(iv, data.toString());
                                    return true;
                                }
                                return false;
                            }
                        });

                        lv.setAdapter(adapter);
                        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> parent, View v, int position,
                                                    long id) {
                                Bundle bundle = new Bundle();
                                for (int i = 0; i < 10; i++) {
                                    bundle.putString(arr1[i], mDataList.get(position).get(arr1[i]).toString());
                                }
                                bundle.putString("pre_fragment","home");

                                manager = getFragmentManager();
                                Fragment_Item_Detail fragment_item_detail = new Fragment_Item_Detail();
                                fragment_item_detail.setArguments(bundle);
                                ft = manager.beginTransaction();
                                ft.replace(R.id.realtabcontent, fragment_item_detail);
                                ft.addToBackStack(null);
                                ft.commit();
                            }
                        });
                    } catch (Exception e) {
                        Log.i("msg", e.toString());
                        e.printStackTrace();
                    }

                }
            }

            ;
        };

        return contentView;
    }

    private void initial() {
        tag_list.add("电子");
        tag_list.add("摄像头");
        tag_list.add("显示器");

        price_list.add("全部");
        price_list.add("<100");
        price_list.add("100-500");
        price_list.add("500-2000");
        price_list.add(">2000");
        spinner_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, tag_list);
        spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        tag_search.setAdapter(spinner_adapter);

        spinner_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, price_list);
        spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        price.setAdapter(spinner_adapter);

        //       request for search result
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hint.setVisibility(View.VISIBLE);
                SimpleAdapter adapter = null;
                lv.setAdapter(adapter);
                tag_value = tag_search.getSelectedItem().toString();
                if ("摄像头".equals(tag_value))
                    tag_value = "camera";
                name_value = name_search.getText().toString();
                long temp = price.getSelectedItemId();

                if (temp == 0) {
                    low_value = "0";
                    high_value = "";
                } else if (temp == 1) {
                    low_value = "0";
                    high_value = "100";
                } else if (temp == 2) {
                    low_value = "100";
                    high_value = "500";
                } else if (temp == 3) {
                    low_value = "500";
                    high_value = "2000";
                } else {
                    low_value = "2000";
                    high_value = "";
                }
//
//                Log.i("msg",get_value+"2");
                search_get(name_value, tag_value, low_value, high_value);
            }
        });
    }


    private List<Map<String, Object>> getDataFromBundle(Bundle b) {
        final List<Map<String, Object>> mDataList = new ArrayList<Map<String, Object>>();
        ArrayList list = b.getStringArrayList("list");
        HashMap<String, Object> map = new HashMap<String, Object>();
        Log.i("msg", "1234567");
        for (int i = 0; i < list.size(); i++) {
            map.put(arr1[i % 10], list.get(i).toString());
            if (i % 10 == 9) {
                Log.i("msg", list.get(i).toString());
                mDataList.add(map);
                map = new HashMap<String, Object>();
            }
        }
        Log.i("msg", mDataList.size() + "");
        return mDataList;
    }

    private void search_get(String name, String tag, String low, String high) {
//        Log.i("msg",content);
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("action", "search");
        if (!tag.isEmpty()) {
            params.put("search_tag", tag);
        }
        if (!name.isEmpty()) {
            params.put("search_name", name);
        }
        if (!low.isEmpty()) {
            params.put("search_price_from", low);
        }
        if (!high.isEmpty()) {
            params.put("search_price_to", high);
        }
        String url = "http://1.12353254.sinaapp.com/main";
//        Log.i("msg",url);
        client.get(url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] arg1, byte[] arg2) {

                ArrayList list = new ArrayList();
                try {
                    JSONArray first = new JSONArray(JsonType.getInstance().decodeUTF8(arg2));
                    int len = first.length();
                    for (int j = 0; j < len; j++) {
                        JSONArray temp = first.getJSONArray(j);
                        for (int i = 0; i < 10; i++)
                            list.add(temp.optString(i));
                    }
                    Message msg = mhandler.obtainMessage();
                    Bundle bundler = new Bundle();
                    bundler.putStringArrayList("list", list);
                    msg.setData(bundler);
                    mhandler.sendMessage(msg);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] arg1, byte[] arg2,
                                  Throwable arg3) {
                Log.i("msg", "failure" + arg3);
            }
        });
    }

}
