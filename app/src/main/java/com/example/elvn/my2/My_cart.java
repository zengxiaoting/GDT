package com.example.elvn.my2;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Calendar;


/**
 * Created by Elvn on 2015/12/31.
 */
public class My_cart {
    private SharedPreferences sharedPreferences;
    private My_cart(){}
    private static class CartHolder{
        private static My_cart instance = new My_cart();
    }
    public static My_cart getInstance(){return CartHolder.instance;}
    private String mycartlist;//goods`s info

    public String getMycartlist(Context c){
        if(mycartlist==null){
            sharedPreferences = c.getSharedPreferences("appconfig",c.MODE_PRIVATE);
            String list = sharedPreferences.getString("CARTLIST", "[]");
            return list;
        }else{
            return mycartlist;
        }

    }
    public String getTime(){
        Calendar c = Calendar.getInstance();
        int y = c.get(Calendar.YEAR);
        int m = c.get(Calendar.MONTH);
        m++;
        int d = c.get(Calendar.DAY_OF_MONTH);
        int h = c.get(Calendar.HOUR_OF_DAY);
        int mi = c.get(Calendar.MINUTE);
        String _time = y+"-"+m+"-"+d+" "+h+":"+mi;
        return _time;
    }
    public void setMycartlist(String m){this.mycartlist = m;}
    public void addMycartList(Context c, String m){
        String my = My_cart.getInstance().getMycartlist(c);
        my = my.substring(0,my.length()-1);
        if(my.length()==1){
            my += m;
        }else {
            my += "," + m;
        }
        my+="]";
        My_cart.getInstance().setMycartlist(my);
        My_cart.getInstance().saveList(c);
    }
    public void saveList(Context c){
        sharedPreferences = c.getSharedPreferences("appconfig",c.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("CARTLIST", mycartlist);
        editor.commit();
    }
//    public int getSize(){return mycartlist.size();}

}
