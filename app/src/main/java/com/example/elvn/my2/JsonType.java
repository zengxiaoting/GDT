package com.example.elvn.my2;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Elvn on 2016/1/3.
 */
public class JsonType {
    public static String simpleMapToJsonStr(Map<String ,String> map){
        if(map==null||map.isEmpty()){
            return "null";
        }
        String jsonStr = "{";
        Set<?> keySet = map.keySet();
        for (Object key : keySet) {
            jsonStr += "\""+key+"\":\""+map.get(key)+"\",";
        }
        jsonStr = jsonStr.substring(0,jsonStr.length()-1);
        jsonStr += "}";
        return jsonStr;
    }
    private JsonType(){}
    private static class CartHolder{
        private static JsonType instance = new JsonType();
    }
    public static JsonType getInstance(){return CartHolder.instance;}
    public  String decodeUTF8(byte[] bytes) {
        String msg = "";
        try {
            msg = new String(bytes, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return msg;
    }

}
