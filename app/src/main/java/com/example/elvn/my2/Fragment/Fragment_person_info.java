package com.example.elvn.my2.Fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.elvn.my2.JsonType;
import com.example.elvn.my2.R;
import com.example.elvn.my2.TabActivity;
import com.example.elvn.my2.customview.SlideSwitch;
import com.example.elvn.my2.slide_activity;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.UnsupportedEncodingException;

import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by Elvn on 2015/11/18.
 */
public class Fragment_person_info extends Fragment {
    String usr, pwd;
    TextView nick;
    TextView user;
    TextView pass;
    TextView mail;
    TextView com;
    TextView addr;
    ImageView sex;
    ImageButton btn_re;
    ImageButton btn_back;
    ImageView di;
    ImageButton btn_modify_nick_sex;
    ImageButton btn_modify_password;
    ImageButton btn_modify_mail;
    ImageButton btn_modify_com;
    ImageButton btn_modify_addr;
    private SharedPreferences sharedPreferences;
    private FragmentManager manager1;
    private FragmentTransaction ft1;
    private ProgressDialog myDialog;
    AlertDialog.Builder dialog;
    TextView title;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.fragment_personal_info, null);
        nick = (TextView) contentView.findViewById(R.id.info_nick);
        user = (TextView) contentView.findViewById(R.id.info_username);
        pass = (TextView) contentView.findViewById(R.id.info_password);
        mail = (TextView) contentView.findViewById(R.id.info_mail);
        com = (TextView) contentView.findViewById(R.id.info_company);
        addr = (TextView) contentView.findViewById(R.id.info_address);
        sex = (ImageView) contentView.findViewById(R.id.info_sex);
        usr = getActivity().getIntent().getExtras().getString("username");
        pwd = getActivity().getIntent().getExtras().getString("password");
        btn_back = (ImageButton)getActivity().findViewById(R.id.backbtn);
        btn_back.setVisibility(View.VISIBLE);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manager1 = getFragmentManager();
                Fragment_info fragment_info = new Fragment_info();
                ft1 = manager1.beginTransaction();
                ft1.setCustomAnimations(R.anim.slide_left_in,R.anim.slide_right_out);
                ft1.replace(R.id.realtabcontent, fragment_info);
                ft1.addToBackStack(null);
                ft1.commit();
            }
        });
        di = (ImageView)getActivity().findViewById(R.id.imageView);
        di.setVisibility(View.VISIBLE);
        title = (TextView) getActivity().findViewById(R.id.title_txt);
        title.setText("个人信息");
        btn_re = (ImageButton) getActivity().findViewById(R.id.refreshbtn);
        btn_re.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog = ProgressDialog.show(getActivity(), "提示",
                        "载入中", true, true);

                initial_info_page();
            }
        });
        myDialog = ProgressDialog.show(getActivity(), "提示",
                "载入中", true, true);
        btn_modify_nick_sex = (ImageButton) contentView.findViewById(R.id.modify_nick_sex);
        btn_modify_nick_sex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_nick_dialog(inflater);
            }
        });
        btn_modify_password = (ImageButton) contentView.findViewById(R.id.modify_password);
        btn_modify_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_password_dialog(inflater);
            }
        });
        btn_modify_mail = (ImageButton) contentView.findViewById(R.id.modify_mail);
        btn_modify_mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_mail_dialog(inflater);
            }
        });
        btn_modify_addr = (ImageButton) contentView.findViewById(R.id.modify_address);
        btn_modify_addr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_addr_dialog(inflater);
            }
        });
        btn_modify_com = (ImageButton) contentView.findViewById(R.id.modify_company);
        btn_modify_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_company_dialog(inflater);
            }
        });

        initial_info_page();
        return contentView;
    }

    private void edit_company_dialog(LayoutInflater inflater) {
        dialog = new AlertDialog.Builder(getActivity());
        final View view = inflater.inflate(R.layout.dialog_modify_company, null);
        final AlertDialog mdialog = dialog.setView(view).create();
        final EditText edCom = (EditText) view.findViewById(R.id.new_com);
        FancyButton ensure = (FancyButton) view.findViewById(R.id.ensure2);
        FancyButton cancel = (FancyButton) view.findViewById(R.id.btncancel2);
        ensure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nn = "";
                nn += edCom.getText().toString().trim();
                JSONObject json = new JSONObject();

                try {
                    json.put("username", usr);
                    json.put("password", pwd);
                    json.put("action", "modify_personal_info");
                    json.put("newcompany", nn);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                AsyncHttpClient client = new AsyncHttpClient();
                String url = "http://1.12353254.sinaapp.com/test";
                try {
                    ByteArrayEntity entity = new ByteArrayEntity(json.toString().getBytes("UTF-8"));
                    client.post(getActivity(), url, entity, "application/json", new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int i, Header[] headers, byte[] bytes) {
                            String state = JsonType.getInstance().decodeUTF8(bytes);
                            if ("1".equals(state)) {
                                initial_info_page();
//                                    slide_activity.initial(getActivity(), getActivity(), usr, pwd);
                            } else {
                                Toast.makeText(getActivity(),
                                        "success but OnFailure" + JsonType.getInstance().decodeUTF8(bytes),
                                        Toast.LENGTH_SHORT).show();
                            }
                            mdialog.dismiss();
                        }

                        @Override
                        public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                            Toast.makeText(getActivity(),
                                    "OnFailure" + JsonType.getInstance().decodeUTF8(bytes),
                                    Toast.LENGTH_SHORT).show();
                            mdialog.dismiss();
                        }
                    });
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mdialog.dismiss();
            }
        });
        mdialog.show();
        mdialog.getWindow().setLayout(1000, 1000);
    }

    private void edit_addr_dialog(LayoutInflater inflater) {
        dialog = new AlertDialog.Builder(getActivity());
        final View view = inflater.inflate(R.layout.dialog_modify_addr, null);
        final AlertDialog mdialog = dialog.setView(view).create();
        final EditText edAddr = (EditText) view.findViewById(R.id.new_addr);
        FancyButton ensure = (FancyButton) view.findViewById(R.id.ensure3);
        FancyButton cancel = (FancyButton) view.findViewById(R.id.btncancel3);
        ensure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nn = "";
                nn += edAddr.getText().toString().trim();
                JSONObject json = new JSONObject();

                try {
                    json.put("username", usr);
                    json.put("password", pwd);
                    json.put("action", "modify_personal_info");
                    json.put("newaddr", nn);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                AsyncHttpClient client = new AsyncHttpClient();
                String url = "http://1.12353254.sinaapp.com/test";
                try {
                    ByteArrayEntity entity = new ByteArrayEntity(json.toString().getBytes("UTF-8"));
                    client.post(getActivity(), url, entity, "application/json", new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int i, Header[] headers, byte[] bytes) {
                            String state = JsonType.getInstance().decodeUTF8(bytes);
                            if ("1".equals(state)) {
                                initial_info_page();
//                                    slide_activity.initial(getActivity(), getActivity(), usr, pwd);
                            } else {
                                Toast.makeText(getActivity(),
                                        "success but OnFailure" + JsonType.getInstance().decodeUTF8(bytes),
                                        Toast.LENGTH_SHORT).show();
                            }
                            mdialog.dismiss();
                        }

                        @Override
                        public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                            Toast.makeText(getActivity(),
                                    "OnFailure" + JsonType.getInstance().decodeUTF8(bytes),
                                    Toast.LENGTH_SHORT).show();
                            mdialog.dismiss();
                        }
                    });
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mdialog.dismiss();
            }
        });
        mdialog.show();
        mdialog.getWindow().setLayout(1000, 1000);
    }

    private void edit_mail_dialog(LayoutInflater inflater) {
        dialog = new AlertDialog.Builder(getActivity());
        final View view = inflater.inflate(R.layout.dialog_modify_mail, null);
        final AlertDialog mdialog = dialog.setView(view).create();
        final EditText edMail = (EditText) view.findViewById(R.id.new_mail);
        FancyButton ensure = (FancyButton) view.findViewById(R.id.ensure4);
        FancyButton cancel = (FancyButton) view.findViewById(R.id.btncancel4);
        ensure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("".equals(edMail.getText().toString().trim()) || edMail.getText().toString() == null) {
                    Toast.makeText(getActivity(),
                            "邮箱不能为空",
                            Toast.LENGTH_SHORT).show();
                } else if (!edMail.getText().toString().contains("@") || !"com".equals(edMail.getText().toString().substring(edMail.getText().toString().length() - 3))) {
                    Toast.makeText(getActivity(),
                            "邮箱格式不正确",
                            Toast.LENGTH_SHORT).show();
                } else {
                    String nn = "";
                    nn += edMail.getText().toString().trim();
                    JSONObject json = new JSONObject();

                    try {
                        json.put("username", usr);
                        json.put("password", pwd);
                        json.put("action", "modify_personal_info");
                        json.put("newemail", nn);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    AsyncHttpClient client = new AsyncHttpClient();
                    String url = "http://1.12353254.sinaapp.com/test";
                    try {
                        ByteArrayEntity entity = new ByteArrayEntity(json.toString().getBytes("UTF-8"));
                        client.post(getActivity(), url, entity, "application/json", new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                                String state = JsonType.getInstance().decodeUTF8(bytes);
                                if ("1".equals(state)) {
                                    initial_info_page();
                                    slide_activity.initial(getActivity(), getActivity(), usr, pwd);
                                } else {
                                    Toast.makeText(getActivity(),
                                            "success but OnFailure" + JsonType.getInstance().decodeUTF8(bytes),
                                            Toast.LENGTH_SHORT).show();
                                }
                                mdialog.dismiss();
                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                Toast.makeText(getActivity(),
                                        "OnFailure" + JsonType.getInstance().decodeUTF8(bytes),
                                        Toast.LENGTH_SHORT).show();
                                mdialog.dismiss();
                            }
                        });
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mdialog.dismiss();
            }
        });
        mdialog.show();
        mdialog.getWindow().setLayout(1000, 1000);
    }


    private void edit_nick_dialog(LayoutInflater inflater) {
        dialog = new AlertDialog.Builder(getActivity());
        final View view = inflater.inflate(R.layout.dialog_modify_nick, null);
        final AlertDialog mdialog = dialog.setView(view).create();
        final EditText edNick = (EditText) view.findViewById(R.id.new_nick);
        final SlideSwitch se = (SlideSwitch) view.findViewById(R.id.sex_change);
        FancyButton ensure = (FancyButton) view.findViewById(R.id.ensure);
        FancyButton cancel = (FancyButton) view.findViewById(R.id.btncancel);
        se.setTag(false);
        se.setSlideListener(new SlideSwitch.SlideListener() {
            @Override
            public void open() {
                se.setTag(true);
            }

            @Override
            public void close() {
                se.setTag(false);
            }
        });
        ensure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nn = "";
                if ("".equals(edNick.getText().toString().trim()) || edNick.getText().toString() == null) {
                    Toast.makeText(getActivity(),
                            "昵称不能为空",
                            Toast.LENGTH_SHORT).show();
                } else {
                    nn += edNick.getText().toString().trim();
                    Boolean flag;
                    flag = (Boolean) se.getTag();
                    Log.i("BugMeNot", "nn is ：" + nn + "flag :" + flag.toString());
                    JSONObject json = new JSONObject();

                    try {
                        json.put("username", usr);
                        json.put("password", pwd);
                        json.put("action", "modify_personal_info");
                        json.put("newnick", nn);
                        String s = "";
                        if (flag) {
                            s += "0";
                        } else {
                            s += "1";
                        }
                        json.put("newsex", s);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    AsyncHttpClient client = new AsyncHttpClient();
                    String url = "http://1.12353254.sinaapp.com/test";
                    try {
                        ByteArrayEntity entity = new ByteArrayEntity(json.toString().getBytes("UTF-8"));
                        client.post(getActivity(), url, entity, "application/json", new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                                String state = JsonType.getInstance().decodeUTF8(bytes);
                                if ("1".equals(state)) {
                                    initial_info_page();
                                    slide_activity.initial(getActivity(), getActivity(), usr, pwd);
                                } else {
                                    Toast.makeText(getActivity(),
                                            "success but OnFailure" + JsonType.getInstance().decodeUTF8(bytes),
                                            Toast.LENGTH_SHORT).show();
                                }


                                mdialog.dismiss();
                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                Toast.makeText(getActivity(),
                                        "OnFailure" + JsonType.getInstance().decodeUTF8(bytes),
                                        Toast.LENGTH_SHORT).show();
                                mdialog.dismiss();
                            }
                        });
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mdialog.dismiss();
            }
        });
        mdialog.show();
        mdialog.getWindow().setLayout(1000, 1000);
    }

    private void edit_password_dialog(LayoutInflater inflater) {
        dialog = new AlertDialog.Builder(getActivity());
        final View view = inflater.inflate(R.layout.dialog_modify_password, null);
        final AlertDialog mdialog = dialog.setView(view).create();
        final EditText edPwd1 = (EditText) view.findViewById(R.id.new_pwd1);
        final EditText edPwd2 = (EditText) view.findViewById(R.id.new_pwd2);
        FancyButton ensure = (FancyButton) view.findViewById(R.id.ensure1);
        FancyButton cancel = (FancyButton) view.findViewById(R.id.btncancel1);

        ensure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("".equals(edPwd1.getText().toString().trim()) || edPwd1.getText().toString() == null
                        || "".equals(edPwd2.getText().toString().trim()) || edPwd2.getText().toString() == null) {
                    Toast.makeText(getActivity(),
                            "昵称不能为空",
                            Toast.LENGTH_SHORT).show();
                } else if (!edPwd1.getText().toString().trim().equals(edPwd2.getText().toString().trim())) {
                    Toast.makeText(getActivity(),
                            "密码不一致！请再次输入",
                            Toast.LENGTH_SHORT).show();
                } else if (edPwd1.getText().toString().trim().length() < 6) {
                    Toast.makeText(getActivity(),
                            "密码不能少于6位哦！",
                            Toast.LENGTH_SHORT).show();
                } else {
                    String nn = "";
                    nn += edPwd1.getText().toString().trim();
                    JSONObject json = new JSONObject();

                    try {
                        json.put("username", usr);
                        json.put("password", pwd);
                        json.put("action", "modify_personal_info");
                        json.put("newpassword", nn);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    AsyncHttpClient client = new AsyncHttpClient();
                    String url = "http://1.12353254.sinaapp.com/test";
                    try {
                        ByteArrayEntity entity = new ByteArrayEntity(json.toString().getBytes("UTF-8"));
                        final String finalNn = nn;
                        client.post(getActivity(), url, entity, "application/json", new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                                String state = JsonType.getInstance().decodeUTF8(bytes);
                                if ("1".equals(state)) {
                                    pwd = finalNn;
                                    initial_info_page();
                                    Log.i("BugMeNot", "info: " + pwd);
                                    slide_activity.initial(getActivity(), getActivity(), usr, pwd);

                                    sharedPreferences = getActivity().getSharedPreferences("appconfig", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putString("PASSWORD", pwd);
                                    editor.commit();
                                } else {
                                    Toast.makeText(getActivity(),
                                            "success but OnFailure" + JsonType.getInstance().decodeUTF8(bytes),
                                            Toast.LENGTH_SHORT).show();
                                }
                                mdialog.dismiss();
                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                Toast.makeText(getActivity(),
                                        "OnFailure" + JsonType.getInstance().decodeUTF8(bytes),
                                        Toast.LENGTH_SHORT).show();
                                mdialog.dismiss();
                            }
                        });
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mdialog.dismiss();
            }
        });
        mdialog.show();
        mdialog.getWindow().setLayout(1000, 1000);
    }


    public void initial_info_page() {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        user.setText(usr);
        int len = pwd.length();
        String str = "";
        for (int i = 0; i < len; i++)
            str += "*";
        pass.setText(str);

        params.put("name", usr);
        params.put("password", pwd);
        params.put("action", "query");
        Log.i("BugMeNot", "username is : " + usr + " pws id: " + pwd);
        String url = "http://1.12353254.sinaapp.com/test";

        client.get(url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                try {
                    JSONArray first = new JSONArray(JsonType.getInstance().decodeUTF8(bytes));
                    JSONArray temp = first.getJSONArray(0);
                    nick.setText(temp.getString(3));

                    sharedPreferences = getActivity().getSharedPreferences("appconfig", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("NICKNAME", temp.getString(3));
                    editor.commit();

                    mail.setText(temp.getString(4));
                    addr.setText(temp.getString(7));
                    com.setText(temp.getString(8));
                    if (temp.getString(6).equals("0")) {
                        sex.setImageResource(R.drawable.female);
                    } else {
                        sex.setImageResource(R.drawable.male);
                    }
                    myDialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                Toast.makeText(getActivity(),
                        "Initial Failed" + JsonType.getInstance().decodeUTF8(bytes),
                        Toast.LENGTH_SHORT).show();
                myDialog.dismiss();
            }
        });
    }
}
