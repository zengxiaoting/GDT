package com.example.elvn.my2.Fragment;

import android.content.DialogInterface;
import android.preference.DialogPreference;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.elvn.my2.R;
import com.example.elvn.my2.customview.MaterialDialog;

import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by Elvn on 2015/11/9.
 */
public class Fragment_info extends Fragment {
    ImageButton btn_re;
    FancyButton profile;
    FancyButton intro;
    FancyButton upgrade;
    FancyButton quit;
    private FragmentManager manager;
    private FragmentTransaction ft;
    private TextView title;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        ImageButton btn_back = (ImageButton)getActivity().findViewById(R.id.backbtn);
        btn_back.setVisibility(View.GONE);
        ImageView di = (ImageView)getActivity().findViewById(R.id.imageView);
        di.setVisibility(View.GONE);
        ImageButton btn_re = (ImageButton)getActivity().findViewById(R.id.refreshbtn);
        btn_re.setVisibility(View.GONE);
        ImageView di2 = (ImageView)getActivity().findViewById(R.id.imageView2);
        di2.setVisibility(View.GONE);
        title = (TextView) getActivity().findViewById(R.id.title_txt);
        title.setText("设置");
        View contentView = inflater.inflate(R.layout.fragment_info, null);

        profile = (FancyButton) contentView.findViewById(R.id.info_profile);
        intro = (FancyButton) contentView.findViewById(R.id.info_desc);
        upgrade = (FancyButton) contentView.findViewById(R.id.info_up);
        quit = (FancyButton) contentView.findViewById(R.id.info_quit);

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manager = getFragmentManager();
                Fragment_person_info fragment_person_info = new Fragment_person_info();
                ft = manager.beginTransaction();
                ft.setCustomAnimations(R.anim.slide_right_in,R.anim.slide_left_out);
                ft.replace(R.id.realtabcontent, fragment_person_info);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        upgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final MaterialDialog mMaterialDialog = new MaterialDialog(getActivity());
                mMaterialDialog.setTitle("软件更新");
                mMaterialDialog.setMessage("下一次可不可以换你褪去一身骄傲，喜欢我到疯掉.");
                mMaterialDialog.setCanceledOnTouchOutside(true);
                mMaterialDialog.setPositiveButton("确认", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                    }
                });
                mMaterialDialog.show();
            }
        });
        intro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final MaterialDialog mMaterialDialog = new MaterialDialog(getActivity());
                mMaterialDialog.setTitle("软件介绍");
                mMaterialDialog.setMessage("很可惜你始终没有看到我为你勇敢的样子.");
                mMaterialDialog.setCanceledOnTouchOutside(true);
                mMaterialDialog.setPositiveButton("确认", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                    }
                });
                mMaterialDialog.show();
            }
        });
        quit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final MaterialDialog mMaterialDialog = new MaterialDialog(getActivity());
                mMaterialDialog.setTitle("确认退出吗");
                mMaterialDialog.setMessage("");
                mMaterialDialog.setCanceledOnTouchOutside(true);
                mMaterialDialog.setPositiveButton("确认", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                        getActivity().finish();
                    }
                });
                mMaterialDialog.setNegativeButton("取消", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                    }
                });

                mMaterialDialog.show();

            }
        });

        return contentView;
    }
}