package com.example.elvn.my2;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.example.elvn.my2.Fragment.Fragment_Item_Detail;
import com.example.elvn.my2.Fragment.Fragment_cycle;
import com.example.elvn.my2.Fragment.Fragment_home;
import com.example.elvn.my2.Fragment.Fragment_info;
import com.example.elvn.my2.Fragment.Fragment_plan;
import com.example.elvn.my2.Fragment.Fragment_search;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;

/**
 * Created by Elvn on 2015/11/9.
 */
public class TabActivity extends FragmentActivity {
    public String username;
    public String password;
    ImageButton refresh;
    private SharedPreferences sharedPreferences;
    private TextView title_txt;
    private FragmentManager manager;
    private FragmentTransaction ft;
    private FragmentTabHost tabHost;
    private LayoutInflater layoutInflater;
    private Class fragmentArray[]={Fragment_home.class, Fragment_plan.class, Fragment_cycle.class,Fragment_info.class,Fragment_search.class,Fragment_Item_Detail.class};
    private int ImageArray[]={R.drawable.home,R.drawable.plans,R.drawable.cycle,R.drawable.setting};
    private String txtArray[]={"首页","解决方案","广电圈","设置"};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.main_tab_layout);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
                R.layout.titlebar);
        refresh = (ImageButton)findViewById(R.id.refreshbtn);
        title_txt = (TextView) findViewById(R.id.title_txt);
        Bundle b = getIntent().getExtras();
        username = b.getString("username");
        password = b.getString("password");

        initview();
        slide_activity.initial(this, this,username,password);
    }

    private void initview() {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("name", username);
        params.put("password", password);
        params.put("action", "query");
        String url = "http://1.12353254.sinaapp.com/test";

        client.get(url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                try {
                    JSONArray first = new JSONArray(JsonType.getInstance().decodeUTF8(bytes));
                    JSONArray temp = first.getJSONArray(0);
                    sharedPreferences = TabActivity.this.getSharedPreferences("appconfig", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("NICKNAME", temp.getString(3));
                    editor.putString("USERNAME", username);
                    editor.putString("PASSWORD", password);
                    editor.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                Toast.makeText(TabActivity.this,
                        "Initial Failed" + JsonType.getInstance().decodeUTF8(bytes),
                        Toast.LENGTH_SHORT).show();
            }
        });

        title_txt.setText(txtArray[0]);
        layoutInflater = LayoutInflater.from(this);
        tabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        tabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);
        TabHost.TabSpec tabSpec = tabHost.newTabSpec(txtArray[0]).setIndicator(getTabItemView(0));
        tabSpec.setContent(new Intent(this, fragmentArray[0]));
        tabHost.addTab(tabSpec, fragmentArray[0], null);
        tabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.bt_tab);
        tabHost.getTabWidget().getChildAt(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh.setImageResource(R.drawable.refresh);
                title_txt.setText(txtArray[0]);
                manager = getSupportFragmentManager();
                Fragment_home fragment_home = new Fragment_home();
                ft = manager.beginTransaction();
                ft.setCustomAnimations(R.anim.zoomin,R.anim.zoomout);
                ft.replace(R.id.realtabcontent, fragment_home);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        tabSpec = tabHost.newTabSpec(txtArray[1]).setIndicator(getTabItemView(1));
        tabSpec.setContent(new Intent(this, fragmentArray[1]));
        tabHost.addTab(tabSpec, fragmentArray[1], null);
        tabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.bt_tab);
        tabHost.getTabWidget().getChildAt(1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh.setImageResource(R.drawable.refresh);
                title_txt.setText(txtArray[1]);
                manager = getSupportFragmentManager();
                Fragment_plan fragment_plan = new Fragment_plan();
                ft = manager.beginTransaction();
                ft.setCustomAnimations(R.anim.zoomin,R.anim.zoomout);
                ft.replace(R.id.realtabcontent, fragment_plan);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        tabSpec = tabHost.newTabSpec(txtArray[2]).setIndicator(getTabItemView(2));
        tabSpec.setContent(new Intent(this, fragmentArray[2]));
        tabHost.addTab(tabSpec, fragmentArray[2], null);
        tabHost.getTabWidget().getChildAt(2).setBackgroundResource(R.drawable.bt_tab);
        tabHost.getTabWidget().getChildAt(2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh.setImageResource(R.drawable.plus);
                title_txt.setText(txtArray[2]);
                manager = getSupportFragmentManager();
                Fragment_cycle fragment_cycle = new Fragment_cycle();
                ft = manager.beginTransaction();
                ft.setCustomAnimations(R.anim.zoomin,R.anim.zoomout);
                ft.replace(R.id.realtabcontent, fragment_cycle);
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        tabSpec = tabHost.newTabSpec(txtArray[3]).setIndicator(getTabItemView(3));
        tabSpec.setContent(new Intent(this, fragmentArray[3]));
        tabHost.addTab(tabSpec, fragmentArray[3], null);
        tabHost.getTabWidget().getChildAt(3).setBackgroundResource(R.drawable.bt_tab);
        tabHost.getTabWidget().getChildAt(3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh.setImageResource(R.drawable.refresh);
                title_txt.setText(txtArray[3]);
                manager = getSupportFragmentManager();
                Fragment_info fragment_info = new Fragment_info();
                ft = manager.beginTransaction();
                ft.setCustomAnimations(R.anim.zoomin,R.anim.zoomout);
                ft.replace(R.id.realtabcontent, fragment_info);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
    }
private View getTabItemView(int index) {
    View view = layoutInflater.inflate(R.layout.tab_button, null);
    ImageView imageView = (ImageView) view.findViewById(R.id.imageview);
    imageView.setImageResource(ImageArray[index]);
    return view;

}
    public void setPassword(String newpwd){
        password = newpwd;
    }


}
