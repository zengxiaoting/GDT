package com.example.elvn.my2.Fragment;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.elvn.my2.JsonType;
import com.example.elvn.my2.R;
import com.example.elvn.my2.customview.ScrollDisabledGridView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import net.tsz.afinal.FinalBitmap;

import org.apache.http.Header;
import org.json.JSONArray;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Elvn on 2015/11/9.
 */
public class Fragment_home extends Fragment {
    LinearLayout btnSearch;
    ImageButton btn_re;
    ImageButton btn_back;
    ImageView di;
    private FragmentManager manager;
    private FragmentTransaction ft;
    private Handler mhandler;
    private ScrollDisabledGridView gv1;
    private ScrollDisabledGridView gv2;
    private ScrollDisabledGridView gv3;
    private TextView title;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(container == null){
            return null;
        }
        btn_re = (ImageButton)getActivity().findViewById(R.id.refreshbtn);
        btn_re.setVisibility(View.VISIBLE);
        ImageView di2 = (ImageView)getActivity().findViewById(R.id.imageView2);
        di2.setVisibility(View.VISIBLE);
        title = (TextView) getActivity().findViewById(R.id.title_txt);
        title.setText("首页");
        final View contentView = inflater.inflate(R.layout.activity_home,null);
        btn_re = (ImageButton) getActivity().findViewById(R.id.refreshbtn);
        btn_re.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("refresh","Fragment_home Refresh");
                home_get();
            }
        });
        btn_back = (ImageButton)getActivity().findViewById(R.id.backbtn);
        btn_back.setVisibility(View.INVISIBLE);
        di = (ImageView)getActivity().findViewById(R.id.imageView);
        di.setVisibility(View.INVISIBLE);
        btnSearch = (LinearLayout) contentView.findViewById(R.id.button_search);
        gv1 = (ScrollDisabledGridView) contentView.findViewById(R.id.gridView1);
        gv2 = (ScrollDisabledGridView) contentView.findViewById(R.id.gridView2);
        gv3 = (ScrollDisabledGridView) contentView.findViewById(R.id.gridView3);
        btnSearch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Bundle b = new Bundle();
                manager = getFragmentManager();
                Fragment_search fragment_search = new Fragment_search();
                ft = manager.beginTransaction();
                ft.setCustomAnimations(R.anim.slide_right_in, R.anim.slide_left_out);
                ft.replace(R.id.realtabcontent, fragment_search);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        home_get();
        mhandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                Bundle bun = msg.getData();
                if(bun.isEmpty())
                    System.out.println("Bundle doesn`t get data");
                else {
                    System.out.println("Bundle gets data");
                    try {
                        setTag1(bun,gv1,"list1");
                        setTag1(bun,gv2,"list2");
                        setTag1(bun,gv3,"list3");
                    }catch(Exception e){
                        e.printStackTrace();
                    }

                }
            };
        };
        return contentView;
    }
    private void setTag1(Bundle b,ScrollDisabledGridView gv,String listname){
        final List<Map<String, Object>> mDataList = new ArrayList<Map<String, Object>>();
        HashMap<String, Object> map = new HashMap<String, Object>();
        final String arr1[]={"item_id","item_name","item_seller_id","item_img","item_price",
                "item_quantity","item_size","item_tag","item_info","item_desc"};
        for(int i=0;i<10;i++){
            map.put(arr1[i], b.getStringArrayList(listname).get(i).toString());
        }
        mDataList.add(map);//first tag`s first item`s data

        map = new HashMap<String, Object>();
        for(int i=0;i<10;i++){
            map.put(arr1[i], b.getStringArrayList(listname).get(i + 10).toString());
        }
        mDataList.add(map);//first tag`s second item`s data

        SimpleAdapter adapter = new SimpleAdapter(getActivity(), mDataList,
                R.layout.home_item,
                new String[]{"item_name", "item_seller_id", "item_img",
                        "item_price", "item_id", "item_quantity", "item_size",
                        "item_tag", "item_info", "item_desc"},
                new int[]{R.id.itemname, R.id.itemsellerid, R.id.itemimg, R.id.itemprice});
        adapter.setViewBinder(new SimpleAdapter.ViewBinder() {
            @Override
            public boolean setViewValue(View view, Object data,
                                        String textRepresentation) {
                if (view instanceof ImageView && data instanceof String) {
                    ImageView iv = (ImageView) view;
                    FinalBitmap bitmap = FinalBitmap.create(getActivity());
                    Log.i("BugMeNot",data.toString());
                    bitmap.display(iv, data.toString());
                    return true;
                }
                return false;
            }
        });
        gv.setAdapter(adapter);
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position,
                                    long id) {
                Bundle bundle = new Bundle();
                for (int i = 0; i < 10; i++) {
                    bundle.putString(arr1[i], mDataList.get(position).get(arr1[i]).toString());
                }
                bundle.putString("pre_fragment", "home");
                manager = getFragmentManager();
                Fragment_Item_Detail fragment_item_detail = new Fragment_Item_Detail();
                fragment_item_detail.setArguments(bundle);
                ft = manager.beginTransaction();
                ft.setCustomAnimations(R.anim.slide_right_in, R.anim.slide_left_out);
                ft.replace(R.id.realtabcontent, fragment_item_detail);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
    }
    private void home_get() {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("catg", "camera|电子|显示器");
        String url = "http://1.12353254.sinaapp.com/main";

        client.get(url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] arg1, byte[] arg2) {
                ArrayList list1 = new ArrayList();
                ArrayList list2 = new ArrayList();
                ArrayList list3 = new ArrayList();

                try {
                    JSONArray first = new JSONArray(JsonType.getInstance().decodeUTF8(arg2));
                    int len = first.length();
                    int f1 = 0, f2 = 0, f3 = 0;
                    for (int j = 0; j < len; j++) {
                        JSONArray temp = first.getJSONArray(j);
                        switch (temp.optString(7)) {
                            case ("camera"):
                                if (f1 < 2) {
                                    f1++;
                                    for (int i = 0; i < 10; i++)
                                        list1.add(temp.optString(i));
                                    break;
                                } else {
                                    j++;
                                    break;
                                }
                            case ("电子"):
                                if (f2 < 2) {
                                    f2++;
                                    for (int i = 0; i < 10; i++)
                                        list2.add(temp.optString(i));
                                    break;
                                } else {
                                    j++;
                                    break;
                                }
                            case ("显示器"):
                                if (f3 < 2) {
                                    f3++;
                                    for (int i = 0; i < 10; i++)
                                        list3.add(temp.optString(i));
                                    break;
                                } else {
                                    j++;
                                    break;
                                }
                            default:
                                break;
                        }
                    }

                    Bundle bundler = new Bundle();
                    bundler.putStringArrayList("list1", list1);
                    bundler.putStringArrayList("list2", list2);
                    bundler.putStringArrayList("list3", list3);
                    Log.i("BugMeNot", list1.size() + " , " + list2.size() + " , " + list3.size());
                    Message msg = mhandler.obtainMessage();
                    msg.setData(bundler);
                    mhandler.sendMessage(msg);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] arg1, byte[] arg2,
                                  Throwable arg3) {
                Toast.makeText(getActivity(), "获取失败！" + statusCode,
                        Toast.LENGTH_SHORT).show();
            }
        });
    }





    private long firstTime = 0;
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch(keyCode)
        {
            case KeyEvent.KEYCODE_BACK:
                long secondTime = System.currentTimeMillis();
                if (secondTime - firstTime > 2000) {                                       //如果两次按键时间间隔大于2秒，则不退出
                    Toast.makeText(getActivity(), "再按一次退出程序", Toast.LENGTH_SHORT).show();
                    firstTime = secondTime;//更新firstTime
                    return true;
                } else {                                                    //两次按键小于2秒时，退出应用
                    System.exit(0);
                }
                break;
        }
        return getActivity().onKeyUp(keyCode, event);
    }
}





















