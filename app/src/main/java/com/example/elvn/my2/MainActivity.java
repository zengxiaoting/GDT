package com.example.elvn.my2;

import java.io.UnsupportedEncodingException;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import mehdi.sakout.fancybuttons.FancyButton;

public class MainActivity extends Activity {
	private EditText username;
	private EditText password;
	private FancyButton login;
	private FancyButton regist;
	private CheckBox checkBox;
	static String account;
	static String user_id;
	String get_username;
	String get_password;
	private ProgressDialog myDialog;
	private SharedPreferences sharedPreferences;
	private Boolean flag;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		flag = false;

		sharedPreferences = getSharedPreferences("appconfig",Context.MODE_PRIVATE);
		initviews();
		if(sharedPreferences.getBoolean("AUTO_LOGIN", false)) {
			checkBox.setChecked(true);
			String userStr = sharedPreferences.getString("USERNAME", "");
			String pwStr = sharedPreferences.getString("PASSWORD", "");
			if (userStr != null && (!userStr.equals(""))) {
				username.setText(userStr);
			}
			if (pwStr != null && (!pwStr.equals(""))) {
				password.setText(pwStr);
			}
		}else{
			checkBox.setChecked(false);
		}
	}

	private void initviews() {
		username = (EditText) findViewById(R.id.editText_usr);
		password = (EditText) findViewById(R.id.editText_pwd);
		login = (FancyButton) findViewById(R.id.loginBtn);
		regist = (FancyButton) findViewById(R.id.signupBtn);
		checkBox = (CheckBox) findViewById(R.id.checkBox);
		login.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				myDialog = ProgressDialog.show(MainActivity.this, "提示",
						"登录中", true, true);
				get_username = username.getText().toString().trim();
				get_password = password.getText().toString().trim();
				if(get_username.equals("")||get_password.equals("")){
					Toast.makeText(MainActivity.this,"账号或密码不能为空",Toast.LENGTH_SHORT).show();
					myDialog.dismiss();
				}
				else {
					if(flag){
						sharedPreferences = getSharedPreferences("appconfig",Context.MODE_PRIVATE);
						SharedPreferences.Editor editor = sharedPreferences.edit();
						editor.putString("USERNAME",get_username);
						editor.putString("PASSWORD",get_password);
						editor.putBoolean("AUTO_LOGIN",flag);
						editor.commit();
					}else{
						sharedPreferences = getSharedPreferences("appconfig",Context.MODE_PRIVATE);
						SharedPreferences.Editor editor = sharedPreferences.edit();
						editor.putString("USERNAME","");
						editor.putString("PASSWORD","");
						editor.putBoolean("AUTO_LOGIN",flag);
						editor.commit();
					}
					login_get(get_username, get_password);

				}
			}
		});

		regist.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				myDialog = ProgressDialog.show(MainActivity.this, "跳转",
						"注册界面", true, true);

				Intent intent = new Intent(MainActivity.this,
						RegisterActivity.class);
				startActivity(intent);
				MainActivity.this.finish();
			}
		});

		checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					flag = true;
				} else {
					flag = false;
				}
			}
		});


	}

	private void login_get(final String username, final String password) {
		AsyncHttpClient client = new AsyncHttpClient();
		RequestParams params = new RequestParams();
		params.put("name", username);
		params.put("password", password);
		String url = "http://1.12353254.sinaapp.com/test";

		client.get(url, params, new AsyncHttpResponseHandler() {

			@Override
			public void onSuccess(int statusCode, Header[] arg1, byte[] arg2) {
				if (JsonType.getInstance().decodeUTF8(arg2).equals("1")) {

					// myDialog.dismiss();
					Toast.makeText(getApplicationContext(), "登入成功",
							Toast.LENGTH_SHORT).show();
					Bundle bun = new Bundle();
					bun.putString("username", username);//todo encryption
					bun.putString("password", password);
					Intent intent = new Intent(MainActivity.this,
							TabActivity.class);
					intent.putExtras(bun);
					startActivity(intent);
					if (myDialog != null && myDialog.isShowing()) {
						myDialog.dismiss();
					}
					MainActivity.this.finish();
				} else {
					Toast.makeText(getApplicationContext(), "登入失败！用户名或密码错误！",
							Toast.LENGTH_SHORT).show();
					if (myDialog != null && myDialog.isShowing()) {
						myDialog.dismiss();
					}
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] arg1, byte[] arg2,
								  Throwable arg3) {
				Toast.makeText(getApplicationContext(), "网络出问题了" + statusCode,
						Toast.LENGTH_SHORT).show();
				myDialog.dismiss();
			}
		});
	}
	private long firstTime = 0;
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		switch(keyCode)
		{
			case KeyEvent.KEYCODE_BACK:
				long secondTime = System.currentTimeMillis();
				if (secondTime - firstTime > 2000) {                                         //如果两次按键时间间隔大于2秒，则不退出
					Toast.makeText(this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
					firstTime = secondTime;//更新firstTime
					return true;
				} else {                                                    //两次按键小于2秒时，退出应用
					System.exit(0);
				}
				break;
		}
		return super.onKeyUp(keyCode, event);
	}

}
