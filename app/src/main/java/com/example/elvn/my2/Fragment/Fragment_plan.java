package com.example.elvn.my2.Fragment;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.example.elvn.my2.JsonType;
import com.example.elvn.my2.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import net.tsz.afinal.FinalBitmap;

import org.apache.http.Header;
import org.json.JSONArray;
import org.w3c.dom.Text;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Elvn on 2015/11/15.
 */
public class Fragment_plan extends Fragment {
    final String arr1[] = {"item_id", "item_name", "item_seller_id", "item_img", "item_price",
            "item_quantity", "item_size", "item_tag", "item_info", "item_desc"};
    ImageButton btn_re;
    TextView group1,group2;
    TextView group2_child1,group2_child2,group2_child3;
    TextView group1_child1,group1_child2,group1_child3;
    boolean flag1,flag2;
    ListView lv;
    View convertView;
    Handler mhandler;
    private FragmentTransaction ft;
    private FragmentManager manager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        convertView = inflater.inflate(R.layout.fragment_plan, null);
        lv = (ListView) convertView.findViewById(R.id.plancontent);
        btn_re = (ImageButton) getActivity().findViewById(R.id.refreshbtn);
        btn_re.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("refresh", "Fragment_plan Refresh");
            }
        });
        TextView title = (TextView) getActivity().findViewById(R.id.title_txt);
        title.setText("解决方案");
        initBtn();
        setGroupListener();
        viewClickListener(group2_child1);
        viewClickListener(group2_child2);
        viewClickListener(group2_child3);
        viewClickListener(group1_child1);
        viewClickListener(group1_child2);
        viewClickListener(group1_child3);
        mhandler = new Handler(){
            @Override
            public void handleMessage(Message msg){
                Bundle b = msg.getData();
                final List<Map<String, Object>> mDataList = getDataFromBundle(b);
                if (b.isEmpty())
                    Log.i("msg", "Bundle doesn`t get data");
                else {
                    try {
                        SimpleAdapter adapter = new SimpleAdapter(getActivity(), mDataList,
                                R.layout.home_item,
                                new String[]{"item_name", "item_seller_id", "item_img",
                                        "item_price", "item_id", "item_quantity", "item_size",
                                        "item_tag", "item_info", "item_desc"},
                                new int[]{R.id.itemname, R.id.itemsellerid, R.id.itemimg, R.id.itemprice});

                        adapter.setViewBinder(new SimpleAdapter.ViewBinder() {
                            @Override
                            public boolean setViewValue(View view, Object data,
                                                        String textRepresentation) {
                                if (view instanceof ImageView && data instanceof String) {
                                    ImageView iv = (ImageView) view;
                                    FinalBitmap bitmap = FinalBitmap.create(getActivity());
                                    bitmap.display(iv, data.toString());
                                    return true;
                                }
                                return false;
                            }
                        });

                        lv.setAdapter(adapter);
                        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> parent, View v, int position,
                                                    long id) {
                                Bundle bundle = new Bundle();
                                for (int i = 0; i < 10; i++) {
                                    bundle.putString(arr1[i], mDataList.get(position).get(arr1[i]).toString());
                                }
                                bundle.putString("pre_fragment","plan");

                                manager = getFragmentManager();
                                Fragment_Item_Detail fragment_item_detail = new Fragment_Item_Detail();
                                fragment_item_detail.setArguments(bundle);
                                ft = manager.beginTransaction();
                                ft.replace(R.id.realtabcontent, fragment_item_detail);
                                ft.addToBackStack(null);
                                ft.commit();
                            }
                        });
                    } catch (Exception e) {
                        Log.i("msg", e.toString());
                        e.printStackTrace();
                    }

                }
            }
        };




        return convertView;
    }

    private void viewClickListener(final TextView textView){
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String content = textView.getText().toString();
                Log.i("BugMeNot", content);
                search(content);
            }
        });
    }

    private void search(String content) {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("action", "search");
        params.put("search_tag", "camera");
        String url = "http://1.12353254.sinaapp.com/main";
        client.get(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] arg1, byte[] arg2) {
                ArrayList list = new ArrayList();
                try {
                    String my = JsonType.getInstance().decodeUTF8(arg2);
                    JSONArray first = new JSONArray(my);
                    int len = first.length();
                    for (int j = 0; j < len; j++) {
                        JSONArray temp = first.getJSONArray(j);
                        for (int i = 0; i < 10; i++)
                            list.add(temp.optString(i));
                    }
                    Message msg = mhandler.obtainMessage();
                    Bundle bundler = new Bundle();
                    bundler.putStringArrayList("list", list);
                    msg.setData(bundler);
                    mhandler.sendMessage(msg);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] arg1, byte[] arg2, Throwable throwable) {
            }
        });
    }

    private void initBtn() {
        flag1 = false;
        flag2 = false;
        group1 = (TextView) convertView.findViewById(R.id.plangroup1);
        group2 = (TextView) convertView.findViewById(R.id.plangroup2);
        group1_child1 = (TextView) convertView.findViewById(R.id.plangroup1_child1);
        group1_child2 = (TextView) convertView.findViewById(R.id.plangroup1_child2);
        group1_child3 = (TextView) convertView.findViewById(R.id.plangroup1_child3);
        group2_child1 = (TextView) convertView.findViewById(R.id.plangroup2_child1);
        group2_child2 = (TextView) convertView.findViewById(R.id.plangroup2_child2);
        group2_child3 = (TextView) convertView.findViewById(R.id.plangroup2_child3);
        setgroup1_gone();
        setgroup2_gone();
    }
    private void setGroupListener(){
        group1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!flag1) {
                    Log.i("BugMeNot", " " + flag1 + " ");
                    setgroup1_show();
                    flag1 = true;
                } else {
                    Log.i("BugMeNot", " " + flag1 + " ");
                    setgroup1_gone();
                    flag1 = false;
                }
            }
        });
        group2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!flag2) {
                    Log.i("BugMeNot", " " + flag2 + " ");
                    setgroup2_show();
                    flag2 = true;
                } else {
                    Log.i("BugMeNot", " " + flag2 + " ");
                    setgroup2_gone();
                    flag2 = false;
                }
            }
        });
    }

    private void setgroup1_gone() {
        group1_child1.setVisibility(View.GONE);
        group1_child2.setVisibility(View.GONE);
        group1_child3.setVisibility(View.GONE);
    }

    private void setgroup2_gone() {
        group2_child1.setVisibility(View.GONE);
        group2_child2.setVisibility(View.GONE);
        group2_child3.setVisibility(View.GONE);
    }
    private void setgroup1_show() {
        group1_child1.setVisibility(View.VISIBLE);
        group1_child2.setVisibility(View.VISIBLE);
        group1_child3.setVisibility(View.VISIBLE);
    }

    private void setgroup2_show() {
        group2_child1.setVisibility(View.VISIBLE);
        group2_child2.setVisibility(View.VISIBLE);
        group2_child3.setVisibility(View.VISIBLE);
    }

    private List<Map<String, Object>> getDataFromBundle(Bundle b) {
        final List<Map<String, Object>> mDataList = new ArrayList<Map<String, Object>>();
        ArrayList list = b.getStringArrayList("list");
        HashMap<String, Object> map = new HashMap<String, Object>();
        Log.i("msg", "1234567");
        for (int i = 0; i < list.size(); i++) {
            map.put(arr1[i % 10], list.get(i).toString());
            if (i % 10 == 9) {
                Log.i("msg", list.get(i).toString());
                mDataList.add(map);
                map = new HashMap<String, Object>();
            }
        }
        Log.i("msg", mDataList.size() + "");
        return mDataList;
    }
}

