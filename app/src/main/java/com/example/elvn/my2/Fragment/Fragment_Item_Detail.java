package com.example.elvn.my2.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.elvn.my2.JsonType;
import com.example.elvn.my2.R;

import net.tsz.afinal.FinalBitmap;

import com.example.elvn.my2.My_cart;
import com.example.elvn.my2.customview.RatingBar;
import com.example.elvn.my2.slide_activity;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by Elvn on 2015/11/10.
 */
public class Fragment_Item_Detail extends Fragment {
    int ratecount;
    String usr;
    String [] params = {"item_name","item_img","item_id","item_seller_id","item_price","item_quantity",
            "item_size","item_tag","item_info","item_desc"};
    String item_name;
    String item_img;
    String item_id;
    String item_seller_id;
    String item_price;
    String item_quantity;
    String item_size;
    String item_tag;
    String item_info;
    String item_desc;
    ImageView general_item_img;
    TextView general_item_name;
    TextView general_item_seller_id;
    TextView general_item_price;
    TextView general_item_quantity;
    TextView general_item_tag;
    TextView general_item_info;
    TextView general_item_desc;
    FancyButton add_cart;
    FancyButton add_rate;
    RatingBar ratingBar;
    TextView rate_num;
    ImageButton back_btn;
    ImageView di;
    TextView title;
    FragmentManager manager;
    private SharedPreferences sharedPreferences;
    private FragmentTransaction ft;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(container == null){
            return null;
        }
        sharedPreferences = getActivity().getSharedPreferences("appconfig", Context.MODE_PRIVATE);
        usr = sharedPreferences.getString("USERNAME", "");
        title = (TextView) getActivity().findViewById(R.id.title_txt);
        title.setText("商品详情");
        back_btn = (ImageButton)getActivity().findViewById(R.id.backbtn);
        back_btn.setVisibility(View.VISIBLE);
        di = (ImageView) getActivity().findViewById(R.id.imageView);
        di.setVisibility(View.VISIBLE);

        View view = inflater.inflate(R.layout.general_item, null);
        Bundle bundle = getArguments();
        String pre_frag = bundle.getString("pre_fragment").toString();
        manager = getFragmentManager();
        if("home".equals(pre_frag)){
            back_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Fragment_home fragment_home = new Fragment_home();
                    ft = manager.beginTransaction();
                    ft.setCustomAnimations(R.anim.slide_left_in, R.anim.slide_right_out);
                    ft.replace(R.id.realtabcontent, fragment_home);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        }else if("plan".equals(pre_frag)){
            back_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Fragment_plan fragment_plan = new Fragment_plan();
                    ft = manager.beginTransaction();
                    ft.setCustomAnimations(R.anim.slide_left_in,R.anim.slide_right_out);
                    ft.replace(R.id.realtabcontent, fragment_plan);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        }

        item_name =  bundle.getString("item_name").toString();
        item_img =  bundle.getString("item_img").toString();
        item_id = bundle.getString("item_id").toString();
        item_seller_id = bundle.getString("item_seller_id").toString();
        item_price = bundle.getString("item_price").toString();
        item_quantity = bundle.getString("item_quantity").toString();
        item_size = bundle.getString("item_size").toString();
        item_tag = bundle.getString("item_tag").toString();
        item_info = bundle.getString("item_info").toString();
        item_desc = bundle.getString("item_desc").toString();

        general_item_img = (ImageView) view.findViewById(R.id.general_item_img);
        general_item_name = (TextView) view.findViewById(R.id.general_item_name);
        general_item_seller_id = (TextView) view.findViewById(R.id.general_item_seller);
        general_item_price = (TextView) view.findViewById(R.id.general_item_price);
        general_item_quantity = (TextView) view.findViewById(R.id.general_item_quantity);
        general_item_tag = (TextView) view.findViewById(R.id.general_item_tag);
        general_item_info = (TextView) view.findViewById(R.id.general_item_info);
        general_item_desc = (TextView) view.findViewById(R.id.general_item_desc);
        add_cart = (FancyButton)view.findViewById(R.id.add_cart);
        ratingBar = (RatingBar) view.findViewById(R.id.ratingbar);
        add_rate = (FancyButton) view.findViewById(R.id.add_rate);
        rate_num = (TextView) view.findViewById(R.id.rate_num);

        ratingBar.setOnRatingChangeListener(new RatingBar.OnRatingChangeListener() {
            @Override
            public void onRatingChange(int RatingCount) {
                rate_num.setText(""+RatingCount);
                ratecount = RatingCount;
            }
        });
        add_rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject json = new JSONObject();
                try {
                    json.put("userid", usr);
                    json.put("shopid", item_id);
                    json.put("grade", ""+ratecount);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                AsyncHttpClient client = new AsyncHttpClient();
                String url = "http://1.12353254.sinaapp.com/grade";
                try {
                    ByteArrayEntity entity = new ByteArrayEntity(json.toString().getBytes("UTF-8"));
                    client.post(getActivity(), url, entity, "application/json", new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int i, Header[] headers, byte[] bytes) {
                            String state = JsonType.getInstance().decodeUTF8(bytes);
                                Toast.makeText(getActivity(),
                                        "评价成功!当前该商品的平均评分为:"+state,
                                        Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                            Toast.makeText(getActivity(),
                                    "OnFailure" + JsonType.getInstance().decodeUTF8(bytes),
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });


        FinalBitmap bitmap = FinalBitmap.create(getActivity());
        bitmap.display(general_item_img, item_img);
        general_item_name.setText(item_name);
        general_item_seller_id.setText(item_seller_id);
        general_item_price.setText(item_price);
        general_item_quantity.setText(item_quantity);
        general_item_tag.setText(item_tag);
        general_item_info.setText(item_info);
        general_item_desc.setText(item_desc);
        add_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject json = new JSONObject();
                try {
                    json.put("username", usr);
                    json.put("goods_name", item_name);
                    json.put("goods_price", item_price);
                    json.put("goods_id", item_id);
                    json.put("goods_seller", item_seller_id);
                    json.put("action", "add");
                    json.put("goods_flag", My_cart.getInstance().getTime());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                AsyncHttpClient client = new AsyncHttpClient();
                String url = "http://1.12353254.sinaapp.com/cart";
                try {
                    ByteArrayEntity entity = new ByteArrayEntity(json.toString().getBytes("UTF-8"));
                    client.post(getActivity(), url, entity, "application/json", new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int i, Header[] headers, byte[] bytes) {
                            String state = JsonType.getInstance().decodeUTF8(bytes);
                            if ("1".equals(state)) {
                                Toast.makeText(getActivity(),
                                        "添加至购物车成功!",
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(),
                                        "success but OnFailure" + JsonType.getInstance().decodeUTF8(bytes),
                                        Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                            Toast.makeText(getActivity(),
                                    "OnFailure" + JsonType.getInstance().decodeUTF8(bytes),
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });


        return view;
    }
}
