package com.example.elvn.my2.customview;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.GridView;


public class ScrollDisabledGridView extends GridView {
	private int mPosition;
	
	public ScrollDisabledGridView(Context context) {
		super(context);
	}
	public ScrollDisabledGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
 
    public ScrollDisabledGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
 
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        final int actionMasked = ev.getActionMasked() & MotionEvent.ACTION_MASK;
 
        if (actionMasked == MotionEvent.ACTION_DOWN) {
            // ��¼��ָ����ʱ��λ��
            mPosition = pointToPosition((int) ev.getX(), (int) ev.getY());
            return super.dispatchTouchEvent(ev);
        }
 
        if (actionMasked == MotionEvent.ACTION_MOVE) {
            // ��ؼ��ĵط�������MOVE �¼�
        	// ListView onTouch��ȡ����MOVE�¼����Բ��ᷢ����������
            return true;
        }
 
        // ��ָ̧��ʱ
        if (actionMasked == MotionEvent.ACTION_UP
        		|| actionMasked == MotionEvent.ACTION_CANCEL) {
            // ��ָ������̧����ͬһ����ͼ�ڣ��������ؼ���������һ������¼�
            if (pointToPosition((int) ev.getX(), (int) ev.getY()) == mPosition) {
                super.dispatchTouchEvent(ev);
            } else {
            	// �����ָ�Ѿ��Ƴ�����ʱ��Item��˵���ǹ�����Ϊ������Item pressed״̬
                setPressed(false);
                invalidate();
                return true;
            }
        }
 
        return super.dispatchTouchEvent(ev);
    }
}

