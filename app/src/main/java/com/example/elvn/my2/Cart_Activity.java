package com.example.elvn.my2;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Elvn on 2016/1/1.
 */
public class Cart_Activity extends Activity {
    private Handler mhandler;
    private MySimpleAdapter adapter;
    List<JSONObject> mycartlist;
    Map<String, JSONObject> checkedList;
    ListView lv;
    TextView tv_sum;
    CheckBox checkall;
    ImageButton back;
    Button delet;
    private int checkNum;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_cart);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
                R.layout.titlebar);
        back = (ImageButton) findViewById(R.id.backbtn);
        back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
        TextView ti = (TextView) findViewById(R.id.title_txt);
        ti.setText("我的购物车");
        lv = (ListView) findViewById(R.id.goods_listview);
        tv_sum = (TextView) findViewById(R.id.item_sum);
        checkall = (CheckBox) findViewById(R.id.item_all);
        delet = (Button) findViewById(R.id.goods_delete);
        String jsonj = My_cart.getInstance().getMycartlist(Cart_Activity.this);

        loadcart();
        mhandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                Bundle bun = msg.getData();
                if (bun.isEmpty())
                    System.out.println("Bundle doesn`t get data");
                else {
                    System.out.println("Bundle gets data");
                    try {
                        String json = bun.getString("content");
                        mycartlist = new ArrayList<>();
                        JSONArray myj = new JSONArray(json);
                        for (int i = 0; i < myj.length(); i++) {
                            JSONArray temp = myj.getJSONArray(i);
                            String jsob = "{\"item_name\":\"" + temp.optString(1) + "\",\"item_price\":\"" + temp.optString(2) + "\",\"item_seller\":\"" + temp.optString(4) + "\",\"item_flag\":\"" + temp.optString(5) + "\"}";
                            JSONObject tempjs = new JSONObject(jsob);
                            mycartlist.add(tempjs);
                        }

                        checkedList = new HashMap<>();
                        adapter = new MySimpleAdapter(Cart_Activity.this, mycartlist);
                        lv.setAdapter(adapter);
                        checkall.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                if (isChecked) {
                                    for (int i = 0; i < mycartlist.size(); i++) {
                                        adapter.getIsSelected().put(i, true);
                                        try {
                                            checkedList.put(mycartlist.get(i).optString("item_flag"), mycartlist.get(i));
                                        } catch (Exception e) {
                                        }
                                    }
                                    // 数量设为list的长度
                                    Log.i("checkedList", " is " + checkedList.size());
                                    checkNum = mycartlist.size();
                                    // 刷新listview和TextView的显示
                                    dataChanged();
                                } else {
                                    for (int i = 0; i < mycartlist.size(); i++) {
                                        if (adapter.getIsSelected().get(i)) {
                                            adapter.getIsSelected().put(i, false);
                                            checkNum--;// 数量减1
                                        }
                                    }
                                    checkedList.clear();
                                    Log.i("checkedList", " is " + checkedList.size());
                                    // 刷新listview和TextView的显示
                                    dataChanged();
                                }
                            }
                        });

                        delet.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v){
                                for(String key:checkedList.keySet()){
                                    JSONObject json = new JSONObject();
                                    try {
                                        sharedPreferences = Cart_Activity.this.getSharedPreferences("appconfig", Context.MODE_PRIVATE);
                                        String usr = sharedPreferences.getString("USERNAME", "");
                                        json.put("username", usr);
                                        json.put("action", "delete");
                                        json.put("goods_flag", key);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    AsyncHttpClient client = new AsyncHttpClient();
                                    String url = "http://1.12353254.sinaapp.com/cart";
                                    try {
                                        ByteArrayEntity entity = new ByteArrayEntity(json.toString().getBytes("UTF-8"));
                                        client.post(Cart_Activity.this, url, entity, "application/json", new AsyncHttpResponseHandler() {
                                            @Override
                                            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                                                try {
                                                    if (JsonType.getInstance().decodeUTF8(bytes).equals("1")) {
                                                        Toast.makeText(Cart_Activity.this,
                                                                "删除成功!",
                                                                Toast.LENGTH_SHORT).show();
                                                        loadcart();
                                                    }
                                                    else{
                                                        Toast.makeText(Cart_Activity.this,
                                                                "删除失败!",
                                                                Toast.LENGTH_SHORT).show();
                                                    }
                                                } catch (Exception e) {
                                                }
                                            }
                                            @Override
                                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                                Toast.makeText(Cart_Activity.this,
                                                        "删除失败!",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });

                        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                MySimpleAdapter.ViewHolder holder = (MySimpleAdapter.ViewHolder) view.getTag();
                                // 改变CheckBox的状态
                                holder.cBox.toggle();
                                // 将CheckBox的选中状况记录下来
                                adapter.getIsSelected().put(position, holder.cBox.isChecked());
                                // 调整选定条目
                                if (holder.cBox.isChecked() == true) {
                                    checkedList.put(mycartlist.get(position).optString("item_flag"), mycartlist.get(position));
                                    Log.i("checkedList", " is " + checkedList.size());
                                    checkNum++;
                                } else {
                                    checkedList.remove(mycartlist.get(position).optString("item_flag"));
                                    checkNum--;
                                }
                                // 用TextView显示
                                tv_sum.setText("" + checkNum);

                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        };


    }

    public void loadcart() {
        JSONObject json = new JSONObject();
        try {
            sharedPreferences = Cart_Activity.this.getSharedPreferences("appconfig", Context.MODE_PRIVATE);
            String usr = sharedPreferences.getString("USERNAME", "");
            json.put("username", usr);
            json.put("action", "query");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AsyncHttpClient client = new AsyncHttpClient();
        String url = "http://1.12353254.sinaapp.com/cart";
        try {
            ByteArrayEntity entity = new ByteArrayEntity(json.toString().getBytes("UTF-8"));
            client.post(Cart_Activity.this, url, entity, "application/json", new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int i, Header[] headers, byte[] bytes) {
                    try {
                        String my = JsonType.getInstance().decodeUTF8(bytes);
                        Bundle bundle = new Bundle();
                        bundle.putString("content", my);
                        Message msg = mhandler.obtainMessage();
                        msg.setData(bundle);
                        mhandler.sendMessage(msg);
                    } catch (Exception e) {
                    }

                }

                @Override
                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void dataChanged() {
        // 通知listView刷新
        adapter.notifyDataSetChanged();
        // TextView显示最新的选中数目
        tv_sum.setText("" + checkNum);
    }

    public class MySimpleAdapter extends BaseAdapter {
        public List<JSONObject> list;
        private HashMap<Integer, Boolean> isSelected;
        private Context context;
        LayoutInflater inflater;

        public MySimpleAdapter(Context context, List<JSONObject> list) {
            super();
            this.list = list;
            this.context = context;
            inflater = LayoutInflater.from(this.context);
            isSelected = new HashMap<Integer, Boolean>();
            init();
        }

        private void init() {
            for (int i = 0; i < list.size(); i++) {
                getIsSelected().put(i, false);
            }
        }

        public HashMap<Integer, Boolean> getIsSelected() {
            return isSelected;
        }

        public void setIsSelected(HashMap<Integer, Boolean> isSelected) {
            this.isSelected = isSelected;
        }

        @Override
        public int getCount() {
            return list == null ? 0 : list.size();
        }

        @Override
        public Object getItem(int location) {
            return list.get(location);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            JSONObject invoice = list.get(position);
            //这个记录Invoice的id用于操作isCheckedMap来更新CheckBox的状态
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.cart_item, null);
                holder.inName = (TextView) convertView.findViewById(R.id.goods_name);
                holder.inMoney = (TextView) convertView.findViewById(R.id.goods_money);
                holder.inSeller = (TextView) convertView.findViewById(R.id.goods_seller);
                holder.cBox = (CheckBox) convertView.findViewById(R.id.goods_check);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            try {
                holder.cBox.setChecked(getIsSelected().get(position));
                holder.inName.setText(invoice.getString("item_name"));
                holder.inSeller.setText(invoice.getString("item_seller"));
                holder.inMoney.setText(invoice.getString("item_price"));
            } catch (Exception e) {
            }
            return convertView;
        }

        public final class ViewHolder {
            public TextView inName;
            public TextView inSeller;
            public TextView inMoney;
            public CheckBox cBox;
        }
    }
}
