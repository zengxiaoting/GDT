package com.example.elvn.my2;

import java.io.UnsupportedEncodingException;

import org.apache.http.Header;
import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.elvn.my2.customview.SlideSwitch;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import mehdi.sakout.fancybuttons.FancyButton;

public class RegisterActivity extends Activity {
	private EditText username;
	private EditText password;
	private EditText repeatPassword;
	private EditText nickname;
	private EditText email;
	private SlideSwitch sex;
	private EditText address;
	private EditText company;
	private FancyButton regist;
	private ProgressDialog myDialog;
	private SharedPreferences sharedPreferences;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		 requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_regist);
		 getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
				 R.layout.titlebar);
		ImageView di = (ImageView) findViewById(R.id.imageView2);
		ImageButton re = (ImageButton) findViewById(R.id.refreshbtn);
		di.setVisibility(View.INVISIBLE);
		re.setVisibility(View.INVISIBLE);
		TextView ti = (TextView)findViewById(R.id.title_txt);
		ti.setText("注册");
		ImageButton back = (ImageButton) findViewById(R.id.backbtn);
		back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(RegisterActivity.this,
						MainActivity.class);
				startActivity(intent);
				RegisterActivity.this.finish();
			}
		});
		username = (EditText) findViewById(R.id.etName);
		password = (EditText) findViewById(R.id.etPassword);
		repeatPassword = (EditText) findViewById(R.id.etRePassword);
		nickname = (EditText) findViewById(R.id.etNickname);
		email = (EditText) findViewById(R.id.etemail);
		sex = (SlideSwitch) findViewById(R.id.sex_switch);
		address = (EditText) findViewById(R.id.address);
		company = (EditText) findViewById(R.id.company);
		regist = (FancyButton) findViewById(R.id.regist);
		sex.setTag(false);
		sex.setSlideListener(new SlideSwitch.SlideListener() {
			@Override
			public void open() {
				sex.setTag(true);
			}

			@Override
			public void close() {
				sex.setTag(false);
			}
		});

		regist.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String get_username;
				String get_password;
				String get_repeatPassword;
				String get_nickname;
				String get_email;
				String get_sex;
				String get_address;
				String get_company;

				get_username = username.getText().toString().trim();
				get_password = password.getText().toString().trim();
				get_repeatPassword = repeatPassword.getText().toString().trim();
				get_nickname = nickname.getText().toString().trim();
				get_email = email.getText().toString().trim();
				if ((Boolean)sex.getTag())
					get_sex = "1";
				else
					get_sex = "0";
				get_address = address.getText().toString().trim();
				get_company = company.getText().toString().trim();
				if (get_username.equals("")) {
					Toast.makeText(getApplicationContext(), "用户名不能为空",
							Toast.LENGTH_SHORT).show();
				} else if (get_password.equals("")) {
					Toast.makeText(getApplicationContext(), "密码不能为空",
							Toast.LENGTH_SHORT).show();
				} else if (!get_password.equals(get_repeatPassword)) {
					Toast.makeText(getApplicationContext(), "密码不一致",
							Toast.LENGTH_SHORT).show();
				} else if (get_nickname.equals("")) {
					Toast.makeText(getApplicationContext(), "昵称不能为空",
							Toast.LENGTH_SHORT).show();
				} else if (get_email.equals("")) {
					Toast.makeText(getApplicationContext(), "邮箱不能为空",
							Toast.LENGTH_SHORT).show();
				} else if (!get_email.contains("@")||!"com".equals(get_email.substring(get_email.length() - 3))) {
					Toast.makeText(getApplicationContext(),
							"邮箱格式不正确",
							Toast.LENGTH_SHORT).show();
				}else {
					myDialog = ProgressDialog.show(RegisterActivity.this,
							"稍等", "注册中", true, true);
					register(get_username, get_password, get_nickname,
							get_email, get_sex, get_address,
							get_company);
				}
			}
		});
	}

	private void register(final String username, final String password,
			final String nickname, final String email,
			final String sex, final String address, final String company) {
		JSONObject jsonparams = new JSONObject();
		try {
			jsonparams.put("name", username);
			jsonparams.put("password", password);
			jsonparams.put("nickname", nickname);
			jsonparams.put("email", email);
			jsonparams.put("picture","");
			jsonparams.put("sex", sex);
			jsonparams.put("address", address);
			jsonparams.put("company", company);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		try {
			ByteArrayEntity entity = new ByteArrayEntity(jsonparams.toString()
					.getBytes("UTF-8"));

			String url = "http://1.12353254.sinaapp.com/test";
			AsyncHttpClient client = new AsyncHttpClient();
			client.post(RegisterActivity.this, url, entity, "application/json",
					new AsyncHttpResponseHandler() {

						@Override
						public void onFailure(int arg0, Header[] arg1,
								byte[] arg2, Throwable arg3) {
							Toast.makeText(getApplicationContext(),
									"OnFailure" + JsonType.getInstance().decodeUTF8(arg2),
									Toast.LENGTH_SHORT).show();
						}

						@Override
						public void onSuccess(int arg0, Header[] arg1,
								byte[] arg2) {
							Toast.makeText(getApplicationContext(),
									"注册成功" + JsonType.getInstance().decodeUTF8(arg2),
									Toast.LENGTH_SHORT).show();
							sharedPreferences = getSharedPreferences("appconfig",MODE_PRIVATE);
							SharedPreferences.Editor editor = sharedPreferences.edit();
							editor.putString("USERNAME", username);
							editor.putString("PASSWORD", password);
							editor.putString("NICKNAME", nickname);
							editor.commit();

							Intent intent = new Intent(RegisterActivity.this,
									MainActivity.class);
							startActivity(intent);
							if (myDialog != null && myDialog.isShowing()) {
								myDialog.dismiss();
							}
							RegisterActivity.this.finish();
						}

					});
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}
