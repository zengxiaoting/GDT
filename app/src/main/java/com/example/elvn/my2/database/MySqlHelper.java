package com.example.elvn.my2.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Elvn on 2016/1/4.
 */
public class MySqlHelper extends SQLiteOpenHelper {
    private final static String DATABASE_NAME = "message.db";
    final static String TABLE_NAME = "message";
    final static int DB_VERSION = 1;
    Context context;

    public MySqlHelper(Context context) {
        super(context,DATABASE_NAME,null,DB_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "create table if not exists "+TABLE_NAME + " (title char(30),author char(30),money char(10)," +
                "startTime char(20),endTime char(20),missionId char(10),missionState char(2))";
        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
