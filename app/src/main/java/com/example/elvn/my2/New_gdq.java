package com.example.elvn.my2;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.UnsupportedEncodingException;
import java.util.Calendar;

import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by Elvn on 2015/12/27.
 */
public class New_gdq extends Activity {
    String Nick,pass;
    EditText content;
    FancyButton send;
    private ProgressDialog myDialog;
    private SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = New_gdq.this.getSharedPreferences("appconfig", Context.MODE_PRIVATE);
        Nick = sharedPreferences.getString("NICKNAME", "");
        pass = sharedPreferences.getString("PASSWORD", "");
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.new_gdq);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
                R.layout.titlebar);
        ImageView di = (ImageView) findViewById(R.id.imageView2);
        ImageButton re = (ImageButton) findViewById(R.id.refreshbtn);
        TextView title_txt = (TextView) findViewById(R.id.title_txt);
        title_txt.setText("发布新的广电圈");
        di.setVisibility(View.INVISIBLE);
        re.setVisibility(View.INVISIBLE);

        ImageButton back = (ImageButton) findViewById(R.id.backbtn);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(New_gdq.this);
                builder.setMessage("确认退出编辑吗？");
                builder.setTitle("提示");
                builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        New_gdq.this.finish();
                    }
                });
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.create().show();
            }
        });

        content = (EditText) findViewById(R.id.gdq_edit);
        send = (FancyButton) findViewById(R.id.gdq_send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = content.getText().toString();
                if (text.equals("")|| text == null) {
                    Toast.makeText(New_gdq.this,
                            "内容不能为空!",
                            Toast.LENGTH_SHORT).show();
                } else{
                    myDialog = ProgressDialog.show(New_gdq.this, "提示",
                            "发送中", true, true);
                JSONObject json = new JSONObject();
                String _time = My_cart.getInstance().getTime();
                Log.i("BugMeNot", _time + "  " + text + "  " + Nick);
                try {
                    json.put("gdq_sender", Nick);
                    json.put("password", pass);
                    json.put("action", "send_New_Gdq");
                    json.put("gdq_text", text);
                    json.put("gdq_time", _time);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                AsyncHttpClient client = new AsyncHttpClient();
                String url = "http://1.12353254.sinaapp.com/friends";
                try {
                    ByteArrayEntity entity = new ByteArrayEntity(json.toString().getBytes("UTF-8"));
                    client.post(New_gdq.this, url, entity, "application/json", new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int i, Header[] headers, byte[] bytes) {
                            String state = JsonType.getInstance().decodeUTF8(bytes);
                            if ("1".equals(state)) {
                                Toast.makeText(New_gdq.this,
                                        "发布成功!",
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(New_gdq.this,
                                        "success but OnFailure" + JsonType.getInstance().decodeUTF8(bytes),
                                        Toast.LENGTH_SHORT).show();
                            }
                            myDialog.dismiss();
                            New_gdq.this.finish();
                        }

                        @Override
                        public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                            Toast.makeText(New_gdq.this,
                                    "OnFailure" + JsonType.getInstance().decodeUTF8(bytes),
                                    Toast.LENGTH_SHORT).show();
                            myDialog.dismiss();
                        }
                    });
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            }
        });

    }
}
