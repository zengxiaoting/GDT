package com.example.elvn.my2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.w3c.dom.Text;

import java.io.UnsupportedEncodingException;

import mehdi.sakout.fancybuttons.FancyButton;

public class slide_activity {
    static TextView username;
    static TextView nickname;
    static TextView mail;
    static FancyButton cart;

    public static void initial(final Activity a, final Context c, String usr, String pwd) {
        SlidingMenu menu = new SlidingMenu(c);
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setShadowDrawable(R.drawable.shadow);
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(a, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.leftmenu);
        username = (TextView) menu.getMenu().findViewById(R.id.menu_username);
        nickname = (TextView) menu.getMenu().findViewById(R.id.menu_Nickname);
        mail = (TextView) menu.getMenu().findViewById(R.id.menu_mail);
        cart = (FancyButton) menu.getMenu().findViewById(R.id.slide_cart);
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(c,Cart_Activity.class);
                c.startActivity(intent);
            }
        });
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("name", usr);
        params.put("password", pwd);
        params.put("action", "query");
        Log.i("BugMeNot0",usr+" "+pwd);
        String url = "http://1.12353254.sinaapp.com/test";

        client.get(url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                try {
                    JSONArray first = new JSONArray(JsonType.getInstance().decodeUTF8(bytes));
                    JSONArray temp = first.getJSONArray(0);
                    username.setText(temp.getString(1));
                    nickname.setText(temp.getString(3));
                    mail.setText(temp.getString(4));

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {

            }
        });
    }

}
