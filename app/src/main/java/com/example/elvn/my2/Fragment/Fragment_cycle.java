package com.example.elvn.my2.Fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.elvn.my2.JsonType;
import com.example.elvn.my2.New_gdq;
import com.example.elvn.my2.R;
import com.example.elvn.my2.RegisterActivity;
import com.example.elvn.my2.customview.MaterialDialog;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.apache.http.Header;
import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Elvn on 2015/11/14.
 */
public class Fragment_cycle extends Fragment {
    String usr, pass;
    final String arr1[] = {"gdq_no", "gdq_text", "gdq_picure", "gdq_sender", "gdq_time", "gdq_comment"};
    ImageButton btn_re;
    ScrollView sv;
    ListView lv;
    private SharedPreferences sharedPreferences;
    String My_nick;
    Handler mhandler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View contentView = inflater.inflate(R.layout.fragment_cycle, null);
        btn_re = (ImageButton) getActivity().findViewById(R.id.refreshbtn);
        btn_re.setVisibility(View.VISIBLE);
        ImageView di2 = (ImageView) getActivity().findViewById(R.id.imageView2);
        di2.setVisibility(View.VISIBLE);
        btn_re = (ImageButton) getActivity().findViewById(R.id.refreshbtn);
        lv = (ListView) contentView.findViewById(R.id.gdq_list);
        btn_re.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),
                        New_gdq.class);
                startActivity(intent);
            }
        });
        TextView title = (TextView) getActivity().findViewById(R.id.title_txt);
        title.setText("广电圈");
        sharedPreferences = getActivity().getSharedPreferences("appconfig", Context.MODE_PRIVATE);
        My_nick = sharedPreferences.getString("NICKNAME", "");
        usr = sharedPreferences.getString("USERNAME", "");
        pass = sharedPreferences.getString("PASSWORD", "");
        init(usr, pass);
        mhandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                Bundle bun = msg.getData();
                if (bun.isEmpty())
                    System.out.println("Bundle doesn`t get data");
                else {
                    Bundle b = msg.getData();
                    final List<Map<String, Object>> mDataList = getDataFromBundle(b);
                    Myadapter adapter = new Myadapter(getActivity(), mDataList);
                    lv.setAdapter(adapter);
                }
            }

            ;
        };
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i("BugMeNot", "Position is " + position);
            }
        });

        return contentView;
    }

    private void init(String user, String pw) {
        String url = "http://1.12353254.sinaapp.com/friends";
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("action", "get_New_Gdq");
        params.put("username", user);
        params.put("password", pw);
        client.get(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] arg1, byte[] arg2) {
                ArrayList list = new ArrayList();
                try {
                    JSONArray first = new JSONArray(JsonType.getInstance().decodeUTF8(arg2));
                    int len = first.length();
                    for (int j = 0; j < len; j++) {
                        JSONArray temp = first.getJSONArray(j);
                        for (int i = 0; i < 6; i++)
                            list.add(temp.optString(i));
                    }
                    Message msg = mhandler.obtainMessage();
                    Bundle bundler = new Bundle();
                    bundler.putStringArrayList("list", list);
                    msg.setData(bundler);
                    mhandler.sendMessage(msg);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                Log.i("msg", "failure" + throwable);

            }
        });

    }

    private List<Map<String, Object>> getDataFromBundle(Bundle b) {
        final List<Map<String, Object>> mDataList = new ArrayList<Map<String, Object>>();
        ArrayList list = b.getStringArrayList("list");
        HashMap<String, Object> map = new HashMap<String, Object>();
        for (int i = 0; i < list.size(); i++) {
            map.put(arr1[i % 6], list.get(i).toString());
            if (i % 6 == 5) {
                Log.i("msg", list.get(i).toString());
                mDataList.add(map);
                map = new HashMap<String, Object>();
            }
        }
        Log.i("msg", mDataList.size() + "");
        return mDataList;
    }


    public class Myadapter extends BaseAdapter {
        List<Map<String, Object>> mData;
        private Context context;
        LayoutInflater inflater;
        int len;

        public Myadapter(Context context, List<Map<String, Object>> list) {
            super();
            this.mData = list;
            this.context = context;
            inflater = LayoutInflater.from(this.context);
            len = list.size();
            len--;
        }

        @Override
        public int getCount() {
            return mData == null ? 0 : mData.size() + 1;
        }

        @Override
        public Object getItem(int position) {
            return mData.get(len - position + 1);
        }

        @Override
        public long getItemId(int position) {
            return position - 1;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (position == 0) {
                convertView = inflater.inflate(R.layout.gdq_header, null);
                TextView n = (TextView) convertView.findViewById(R.id.gdq_myname);
                n.setText(My_nick);
                return convertView;
            }
            Map<String, Object> list = mData.get(len - position + 1);
            ViewHolder h = null;
            String comment_ = list.get("gdq_comment").toString();
            final String gdq_id = list.get("gdq_no").toString();

//            if (convertView == null || convertView.getTag() == null) {
                convertView = inflater.inflate(R.layout.gdq_item, null);
                h = new ViewHolder();
                h.sender = (TextView) convertView.findViewById(R.id.gdq_item_sender);
                h.content = (TextView) convertView.findViewById(R.id.gdq_item_content);
                h.time = (TextView) convertView.findViewById(R.id.gdq_item_time);
                h.comment = (ImageButton) convertView.findViewById(R.id.comment);
                h.comment_area = (LinearLayout) convertView.findViewById(R.id.comment_area);

                if (comment_ == null || comment_ == "null" || comment_ == "") {
                    h.comment_area.removeAllViews();
                    h.comment_area.setVisibility(View.GONE);
                } else {
                    try {
                        JSONArray commentJs = new JSONArray(comment_);
                        int i = commentJs.length();
                        for (int l = 0; l < i; l++) {
                            JSONObject com = commentJs.getJSONObject(l);
                            String content = com.getString("username") + ": " + com.getString("comment");
                            TextView gdq_com = new TextView(getActivity());
                            gdq_com.setText(content);
                            gdq_com.setTextColor(Color.parseColor("#878787"));
                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
                            layoutParams.setMargins(10,2,0,2);
                            gdq_com.setLayoutParams(layoutParams);
                            h.comment_area.addView(gdq_com);
                        }

                    } catch (Exception e) {
                    }
                }
                convertView.setTag(h);
//            } else {
//                h = (ViewHolder) convertView.getTag();
//            }
            h.time.setText(list.get("gdq_time").toString());
            h.sender.setText(list.get("gdq_sender").toString());
            h.content.setText(list.get("gdq_text").toString());
            h.comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final MaterialDialog materialDialog = new MaterialDialog(getActivity());
                    final View view = inflater.inflate(R.layout.new_comment_dialog, null);
                    final MaterialEditText et = (MaterialEditText) view.findViewById(R.id.new_comment);
                    sharedPreferences = getActivity().getSharedPreferences("appconfig", Context.MODE_PRIVATE);
                    materialDialog.setTitle("评论:").setContentView(view).setPositiveButton("确定", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String content = et.getText().toString();
                            if (content.equals("")  || content == null){
                                Toast.makeText(getActivity(),
                                        "评论不能为空!",
                                        Toast.LENGTH_SHORT).show();
                            }else{
                                JSONObject json = new JSONObject();
                                try {
                                    json.put("username", My_nick);
                                    json.put("target_gdq", gdq_id);
                                    json.put("comment", content);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                AsyncHttpClient client = new AsyncHttpClient();
                                String url = "http://1.12353254.sinaapp.com/comment";
                                try {
                                    ByteArrayEntity entity = new ByteArrayEntity(json.toString().getBytes("UTF-8"));
                                    client.post(getActivity(), url, entity, "application/json", new AsyncHttpResponseHandler() {
                                        @Override
                                        public void onSuccess(int i, Header[] headers, byte[] bytes) {
                                            if (JsonType.getInstance().decodeUTF8(bytes).equals("1")) {
                                                Toast.makeText(getActivity(),
                                                        "评论成功!",
                                                        Toast.LENGTH_SHORT).show();
                                                init(usr, pass);
                                            } else {
                                                Toast.makeText(getActivity(),
                                                        "评论失败!" + JsonType.getInstance().decodeUTF8(bytes),
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                            materialDialog.dismiss();
                                        }

                                        @Override
                                        public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                            Toast.makeText(getActivity(),
                                                    "评论失败!" + JsonType.getInstance().decodeUTF8(bytes),
                                                    Toast.LENGTH_SHORT).show();
                                            materialDialog.dismiss();
                                        }
                                    });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }).setNegativeButton("取消", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            materialDialog.dismiss();
                        }
                    });
                    materialDialog.show();

                }
            });

            return convertView;
        }

        public final class ViewHolder {
            public TextView sender;
            public TextView content;
            public TextView time;
            public ImageButton comment;
            public LinearLayout comment_area;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        init(usr, pass);
    }
}
